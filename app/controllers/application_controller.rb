class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_user


  def set_seo_meta(title = '',meta_keywords = '', meta_description = '')
    if title.length > 0
      @page_title = "#{title} &raquo; " + 'eftpos.tk'
    end
    @meta_keywords = meta_keywords
    @meta_description = meta_description
  end

  private 
  
  def authenticate 
    authenticate_or_request_with_http_basic do |user_name, password| 
      user_name == 'shawn' && password == 'ok100fen'
    end 
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  def require_user_login
      unless current_user
        store_location
        flash[:notice] = "You must be logged in to access this page"
        redirect_to login_path
        return false
      end
  end

  def require_user_not_login
      if current_user
        store_location
        flash[:notice] = "You must be logged out to access this page"
        redirect_to root_path
        return false
      end
  end

  def store_location
      session[:return_to] = request.request_uri
  end
end
