# coding: utf-8

class MetaController < ApplicationController
  def about
    respond_to do |format|
      format.html
    end
  end
end