# coding: utf-8

class ShopsController < ApplicationController

  before_filter :authenticate, :only => [:destroy]
  before_filter :require_user_login, :only => [:edit, :update]

  # GET /shops
  # GET /shops.xml
  def index
    if params[:search]
      set_seo_meta('包含 ' + params[:search] + ' 的搜索结果', '包含' + params[:search] + '的eftpos名字')
      @shops = Shop.find(:all, :conditions => ['UPPER(posname) like ? or UPPER(realname) like ?',"%#{params[:search].upcase}%","%#{params[:search].upcase}%"]).paginate :page => params[:page], :per_page =>10
    else
      @shops = Shop.all.paginate :page => params[:page], :per_page => 10
      set_seo_meta('所有商店')
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @shops }
      end
    end
  end

  # GET /shops/1
  # GET /shops/1.xml
  def show 
    @shop = Shop.find(params[:id])
    set_seo_meta(@shop.realname + ' &raquo; ' + @shop.posname, @shop.realname + ' '+ @shop.posname)
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @shop }
    end
  end

  # GET /shops/new
  # GET /shops/new.xml
  def new
    @shop = Shop.new
    set_seo_meta('添加新商店')
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @shop }
    end
  end

  # GET /shops/1/edit
  def edit
    @shop = Shop.find(params[:id])
    set_seo_meta('编辑 &raquo; ' + @shop.realname + ' &raquo; ' + @shop.posname, @shop.realname + ' '+ @shop.posname)
  end

  # POST /shops
  # POST /shops.xml
  def create
    @shop = Shop.new(params[:shop])

    respond_to do |format|
      if @shop.save
        format.html { redirect_to(@shop, :notice => "商店添加成功") }
        format.xml  { render :xml => @shop, :status => :created, :location => @shop }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @shop.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /shops/1
  # PUT /shops/1.xml
  def update
    @shop = Shop.find(params[:id])

    respond_to do |format|
      if @shop.update_attributes(params[:shop])
        format.html { redirect_to(@shop, :success => "商店更新成功") }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shop.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /shops/1
  # DELETE /shops/1.xml
  def destroy
    @shop = Shop.find(params[:id])
    @shop.destroy

    respond_to do |format|
      format.html { redirect_to(shops_url) }
      format.xml  { head :ok }
    end
  end
end