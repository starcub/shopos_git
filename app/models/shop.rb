# coding: utf-8

class Shop < ActiveRecord::Base
  validates_presence_of :posname, :realname, :message=>"不能为空"
  #validates_uniqueness_of :posname, :realname, :case_sensitive => false, :message=>"已经有相同的名字存在了,请检查是否重复添加"
  validates_uniqueness_of :realname, :case_sensitive => false, :message=>"已经有相同的名字存在了,请检查是否重复添加"
  belongs_to :category
  self.per_page = 3


#  def to_param
#    "#{id}-#{realname.parameterize}"
#  end

end