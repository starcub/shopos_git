class AddContactsToShops < ActiveRecord::Migration
  def self.up
    add_column :shops, :fax, :string
    add_column :shops, :cell, :string
    add_column :shops, :email, :string
    add_column :shops, :url, :string
  end

  def self.down
    remove_column :shops, :url
    remove_column :shops, :email
    remove_column :shops, :cell
    remove_column :shops, :fax
  end
end
