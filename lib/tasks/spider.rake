# coding:utf-8
desc "Fetch product prices"
task :spider => :environment do
  require 'rubygems'
  require 'nokogiri'
  require 'open-uri'
  #require 'Iconv'
  # 设置url
  # url format http://opage.skykiwi.com/shop/shop.php?shopid=2830
  #if ARGV.length == 1
  #  url = ARGV[0]
  #else
    docURL = 'http://opage.skykiwi.com/shop/shop.php?shopid='
  #end
  phone=''
  fax=''
  cell=''
  email=''
  address=''
  url=''
  other=''
  for i in 5351..6000 do
    doc = Nokogiri::HTML(open(docURL+i.to_s))
    name = doc.xpath('//h2').inner_text
    #name = Iconv.iconv("GB2312","UTF-8",name)
    
    if doc.css('#s_con_info p').size == 0
      next
    end
    doc.css('#s_con_info p').each do |item|
      if item.css('span')[0].text == '电话：'
        phone = item.css('span')[1].text.strip
        #puts phone
      end
      if item.css('span')[0].inner_text == '传真：'
        fax = item.css('span')[1].text.strip
        #puts fax
      end
      if item.css('span')[0].inner_text == '手机：'
        cell = item.css('span')[1].text.strip
        #puts cell
      end
      if item.css('span')[0].inner_text == 'Email：'
        email = item.css('span')[1].text.strip
        #puts email
      end
      if item.css('span')[0].inner_text == '地址：'
        address = item.css('span')[1].text.strip
        #puts address
      end
      if item.css('span')[0].inner_text == '网址：'
        url = item.css('span')[1].text.strip
        #puts url
      end
#      if item.css('span')[0].inner_text == '商铺介绍'
#        other = item.css('>p span')
#      end
    end
    Shop.create(:posname=>'未知',:realname =>name, :phone=>phone, :fax=>fax, :cell=>cell, :email=>email, :address=>address,:url=>url,:other=>' ',:category_id=>2)
    if i % 100 == 0
      puts i
    end
    #puts shop.inspect
  end
end